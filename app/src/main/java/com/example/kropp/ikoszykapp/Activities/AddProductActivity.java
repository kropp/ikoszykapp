package com.example.kropp.ikoszykapp.Activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.Contacts;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.kropp.ikoszykapp.DB.ProductDatabaseHelper;
import com.example.kropp.ikoszykapp.DB.ShopDatabaseHelper;
import com.example.kropp.ikoszykapp.R;

import java.util.ArrayList;
import java.util.List;

public class AddProductActivity extends AppCompatActivity {

    private ProductDatabaseHelper myDB;     // BAZA DANYCH PRODUKTÓW
    private ShopDatabaseHelper myShopsDB;   // BAZA DANYCH SKLEPÓW

    private String selected_shop;           // ZMIENNA PRZECHOWUJĄCA NAZWĘ ZAZNACZONEGO SKLEPU Z LISTY
    private ArrayList<String> Shops = new ArrayList<>();    // LISTA NAZW SKLEPÓW
    private ListView shopslist;             // POLE PRZECHOWUJĄCE WSZYSTKIE SKLEPY

    private Button addBtn, returnBtn;       // PRZYCISKI DODAJ, ANULUJ
    private EditText name, price;           // POLA DO WPROWADZANIA NAZWY PRODUKTU I CENY

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        myDB = new ProductDatabaseHelper(this);
        myShopsDB = new ShopDatabaseHelper(this);

        name = (EditText) findViewById(R.id.productNameText);
        price = (EditText) findViewById(R.id.productPriceText);
        addBtn = (Button) findViewById(R.id.addProduct);
        returnBtn = (Button) findViewById(R.id.cancelButton);

        shopslist = (ListView) findViewById(R.id.ShopsListView);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, Shops);
        shopslist.setAdapter(arrayAdapter);

        shopslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3) {
                String selectedShop = Shops.get(position);
                Toast.makeText(getApplicationContext(), "Wybrany sklep: " + selectedShop, Toast.LENGTH_LONG).show();
                selected_shop = selectedShop;
            }
        });

        addProduct();
        GoBack();
        getShops();
    }

    // ------------------------------------- DODAWANIE NOWEGO PRODUKTU ------------------------------------- //

    public void addProduct() {
        addBtn.setOnClickListener(add());
    }

    @NonNull
    private View.OnClickListener add() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isProductAdded = myDB.insertProduct(name.getText().toString(), price.getText().toString(), selected_shop);
                if (isProductAdded == true && name.getText() != null && price.getText() != null && selected_shop != null) {
                    Toast.makeText(AddProductActivity.this, "Pomyślnie dodano nowy produkt.", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(AddProductActivity.this, MapsActivity.class);
                    startActivity(intent);
                } else if(name.getText().toString().equals("")){
                    Toast.makeText(AddProductActivity.this, "Wprowadź nazwę produktu.", Toast.LENGTH_LONG).show();
                } else if(price.getText().toString().equals("")){
                    Toast.makeText(AddProductActivity.this, "Wprowadź cenę produktu.", Toast.LENGTH_LONG).show();
                } else if(selected_shop == null){
                    Toast.makeText(AddProductActivity.this, "Wybierz sklep.", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(AddProductActivity.this, "Błąd podczas dodawania produktu.", Toast.LENGTH_LONG).show();
                }
            }
        };
    }

    // ------------------------------------ POWRÓT DO MAPY (ANULUJ) --------------------------------------- //

    public void GoBack() {
        returnBtn.setOnClickListener(returnToMap());
    }

    @NonNull
    private View.OnClickListener returnToMap() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddProductActivity.this, MapsActivity.class);
                startActivity(intent);
                finish();
            }
        };
    }

    // ---------------------------- POBIERANIE NAZW WSZYSTKICH SKLEPÓW Z BAZY ------------------------------ //

    public void getShops() {
        Cursor res = myShopsDB.viewAllShops();
            if (res.getCount() == 0) {
                Toast.makeText(AddProductActivity.this, "Brak sklepów w bazie.", Toast.LENGTH_SHORT).show();
            }
            while (res.moveToNext()) {
                Shops.add(res.getString(1));
            }
    }


}

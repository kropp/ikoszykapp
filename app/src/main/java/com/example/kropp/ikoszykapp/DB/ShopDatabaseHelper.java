package com.example.kropp.ikoszykapp.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Kropp on 2016-01-23.
 */
public class ShopDatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "shop.db";

    public ShopDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 9);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    // ---------------------- TWORZENIE TABLELI SKLEPÓW --------------------------- //

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE shop_table ( ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, LAT TEXT, LNG TEXT,  )");
    }

    // ---------------------- CZYSZCZENIE TABELI SKLEPÓW--------------------------- //

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS shop_table");
        onCreate(db);
    }

    // ---------------------- USUWANIE SKLEPU Z BAZY --------------------------- //

    public void onDelete(String name) {
        SQLiteDatabase exp = this.getWritableDatabase();
        exp.execSQL("DELETE FROM shop_table WHERE NAME= '" + name + "'");
    }

    // ---------------------- DODWANIE NOWEGO SKLEPU DO BAZY --------------------------- //

    public boolean insertShop(String name, String LNG, String LAT){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("NAME", name);
        values.put("LAT", LAT);
        values.put("LNG", LNG);
        long result = db.insert("shop_table", null, values);
        if(result == -1)
            return false;
        else
            return true;
    }

    // ---------------------- POBIERANIE WSZYSTKICH SKLEPÓW --------------------------- //

    public Cursor viewAllShops() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM shop_table", null);
        return res;
    }
}

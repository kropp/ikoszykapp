package com.example.kropp.ikoszykapp.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.kropp.ikoszykapp.Models.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kropp on 2016-01-23.
 */
public class ProductDatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "product.db";

    public ProductDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 9);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    // ---------------------- TWORZENIE TABLELI PRODUKTÓW --------------------------- //

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE product_table ( ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, PRICE TEXT, SHOP TEXT )");
    }

    // -------------------------- USUWANIE PRODUKTU Z BAZY --------------------------- //

    public void onDelete(String name) {
        SQLiteDatabase exp = this.getWritableDatabase();
        exp.execSQL("DELETE FROM product_table WHERE NAME = '" + name +"'");
    }

    public void onDeleteAll(String name) {
        SQLiteDatabase exp = this.getWritableDatabase();
        exp.execSQL("DELETE FROM product_table WHERE SHOP = '" + name +"'");
    }

    // ------------------ CZYSZCZENIE TABELI PRODUKTÓW ---------------------------------- //

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS product_table");
        onCreate(db);
    }

    // ----------------------- DODAWANIE PRODUKTU DO BAZY ---------------------------- //

    public boolean insertProduct(String name, String price, String Shop){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("NAME", name);
        values.put("PRICE", price);
        values.put("SHOP", Shop);
        long result = db.insert("product_table", null, values);
        if(result == -1)
            return false;
        else
            return true;
    }


    // ------------------- POBIERANIE WSZYSTKICH PRODUKTÓW Z BAZY ------------------- //

    public Cursor viewAllProducts() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM product_table", null);
        return res;
    }
}

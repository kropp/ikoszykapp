package com.example.kropp.ikoszykapp.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kropp.ikoszykapp.DB.ShopDatabaseHelper;
import com.example.kropp.ikoszykapp.R;

public class AddShopActivity extends Activity {

    private ShopDatabaseHelper myDB;    // BAZA DANYCH SKLEPÓW
    private TextView lat, lng;          // ZMIENNE DO WYŚWIETLANIA WSPÓŁRZĘDNYCH
    private Button addBtn;              // PRZYCISK DODAJ SKLEP
    private EditText name;              // POLE DO WPROWADZANIA NAZWY SKLEPU
    private String latitude,longitude;  // ZMIENNE DO PRZECHOWYWANIA WSPÓŁRZĘDNYCH

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_shop);
        myDB = new ShopDatabaseHelper(this);

        lat = (TextView) findViewById(R.id.LatText);
        lng = (TextView) findViewById(R.id.LngText);
        addBtn = (Button) findViewById(R.id.AddShopButton);
        name = (EditText) findViewById(R.id.ShopNameEditText);

        // -------- POBIERANIE PRZEKAZANYCH DANYCH I PRZYPISYWANIE ICH DO ZMIENNYCH --------------- //
        Bundle b = getIntent().getExtras();
        latitude = new String(b.getDouble("nlat") + "");
        longitude = new String(b.getDouble("nlng") + "");

        lat.setText("Dł. geogr.   " + latitude);
        lng.setText("Szer. geogr. " + longitude);

        addShop();
    }

    // -------------- DODAWANIE NOWEGO SKLEPU ----------------------- //
    public void addShop() {
        addBtn.setOnClickListener(add());
    }

    @NonNull
    private View.OnClickListener add() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isShopAdded = myDB.insertShop(name.getText().toString(), latitude.toString(), longitude.toString());
                if (isShopAdded == true) {
                    Toast.makeText(AddShopActivity.this, "Pomyślnie dodano nowy sklep.", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(AddShopActivity.this,MapsActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(AddShopActivity.this, "Błąd podczas dodawania sklepu.", Toast.LENGTH_LONG).show();
                }
            }
        };
    }
}

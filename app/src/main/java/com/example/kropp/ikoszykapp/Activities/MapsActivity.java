package com.example.kropp.ikoszykapp.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.kropp.ikoszykapp.DB.ProductDatabaseHelper;
import com.example.kropp.ikoszykapp.DB.ShopDatabaseHelper;
import com.example.kropp.ikoszykapp.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnMarkerDragListener {

    private ShopDatabaseHelper myshopDB;                        // BAZA DANYCH SKLEPÓW
    private ProductDatabaseHelper myProductDB;                  // BAZA DANYCH PRODUKTÓW
    private GoogleMap mMap;                             // MAPA
    private ArrayList<String> produktlist = new ArrayList<>();  // LISTA PRODUKTÓW
    private ArrayAdapter<String> adapter;                       // ADAPTER DO LISTY PRODUKTÓW
    private ListView produkty;                                  // LISTVIEW PRZECHOWUJĄCY PRODUKTY
    private String product_title;                               // NAZWA PRODUKTU
    private Button addShopButton, productsButton, shopsButton, fragmentsButton;

    private Double nlat;                                        // WSPÓŁRZĘDNA ZAZNACZONEGO PUNKTU
    private Double nlng;                                        // WSPÓŁRZĘDNA ZAZNACZONEGO PUNKTU

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, android.R.id.text1, produktlist);
        myshopDB = new ShopDatabaseHelper(this);
        myProductDB = new ProductDatabaseHelper(this);

        addShopButton = (Button) findViewById(R.id.NewShopButton);
        productsButton = (Button) findViewById(R.id.ProductsButton);
        shopsButton = (Button) findViewById(R.id.ShopsButton);
        fragmentsButton = (Button) findViewById(R.id.fragments_button);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        addShop();
        showAllShops();
        goToFragments();
        showAllProducts();
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMarkerDragListener(this);

        LatLng home = new LatLng(54.380891, 18.605193);
        Cursor markers = myshopDB.viewAllShops();

        mMap.addMarker(new MarkerOptions().position(home).title("DOM"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(home, 16.0f));

        // ------------------------ POBIERANIE MARKERÓW Z BAZY ----------------------------- //

        if (markers.getCount() != 0) {
            mMap.addMarker(new MarkerOptions().position(home).title("DOM"));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(home, 16.0f));

            while (markers.moveToNext()) {
                LatLng marker = new LatLng(Double.parseDouble(markers.getString(3)), Double.parseDouble(markers.getString(2)));
                mMap.addMarker(new MarkerOptions().position(marker).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).draggable(true).title(markers.getString(1)));
            }
        }

        // ---------------------- METODA PO KLIKNIĘCIU NA MAPĘ ------------------------------ //

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                Cursor markers = myshopDB.viewAllShops();

                googleMap.clear();
                LatLng home = new LatLng(54.380891, 18.605193);

                mMap.addMarker(new MarkerOptions().position(home).title("DOM"));
                while (markers.moveToNext()) {
                    LatLng marker = new LatLng(Double.parseDouble(markers.getString(3)), Double.parseDouble(markers.getString(2)));
                    mMap.addMarker(new MarkerOptions().position(marker).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).title(markers.getString(1)));
                }

                mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)).title("NOWY PUNKT"));
                nlat = latLng.latitude;
                nlng = latLng.longitude;
            }
        });

    }
        // ----------------- PRZYCISK DODAJ SKLEP ----------------------------------------- //

        public void addShop(){
                addShopButton.setOnClickListener(addNewShop());
        }

        private View.OnClickListener addNewShop() {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(nlat != null && nlng != null){
                        Intent intent = new Intent(MapsActivity.this, AddShopActivity.class);
                        Bundle b = new Bundle();
                        b.putDouble("nlat", nlat); // PRZEKAZYWANIE LAT DO INNEGO ACTIVITY
                        b.putDouble("nlng", nlng); // PRZEKAZYWANIE LNG DO INNEGO ACTIVITY
                        intent.putExtras(b);
                        startActivity(intent);
                    }
                }
            };
        }


        // ----------------- PRZYCISK PRODUKTY ------------------------------------------- //

    public void showAllProducts(){
        productsButton.setOnClickListener(getAllProducts());
    }

    private View.OnClickListener getAllProducts() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cursor res = myProductDB.viewAllProducts();
                if (res.getCount() == 0) {
                    showProducts("Error", "Brak produktów w bazie.");
                    return;
                }

                StringBuffer buffer = new StringBuffer();

                while (res.moveToNext()) {
                    buffer.append("ID :" + res.getString(0) + " \n");
                    buffer.append("Nazwa : " + res.getString(1) + " \n");
                    buffer.append("Koszt : " + res.getDouble(2) + " \n");
                    buffer.append("Sklep : " + res.getString(3) + " \n\n");
                }

                showProducts("Produkty", buffer.toString());

            }
        };
    }

        // ------------------------------- PRZYCISK SKLEPY ------------------------------- //

    public void showAllShops(){
        shopsButton.setOnClickListener(getAllShops());
    }

    private View.OnClickListener getAllShops() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cursor res = myshopDB.viewAllShops();
                if (res.getCount() == 0) {
                    showShops("Error", "Brak sklepów w bazie.");
                    return;
                }

                StringBuffer buffer = new StringBuffer();
                while (res.moveToNext()) {
                    buffer.append("ID    : " + res.getString(0) + " \n");
                    buffer.append("Nazwa : " + res.getString(1) + " \n");
                    buffer.append("LAT   : " + res.getString(2) + " \n");
                    buffer.append("LNG   : " + res.getString(3) + " \n\n");
                }

                showShops("Sklepy", buffer.toString());
            }
        };
    }

    // --------------------- PRZYCISK F ------------------------------------------ //

    public void goToFragments(){
        fragmentsButton.setOnClickListener(startFragmentsActivity());
    }

    private View.OnClickListener startFragmentsActivity() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapsActivity.this, FragmentsActivity.class);
                startActivity(intent);
            }
        };
    }

    // ------------ ALERT DIALOG WYŚWIETLAJĄCY WSZYSTKIE SKLEPY -------------------------- //

    public void showShops(String title, String Message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }

    // ------------ ALERT DIALOG WYŚWIETLAJĄCY WSZYSTKIE PRODUKTY ------------------------ //

    public void showProducts(String title, String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);

        builder.setNeutralButton("Dodaj produkt", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(MapsActivity.this, AddProductActivity.class);
                startActivity(intent);
            }
        });
        builder.show();
    }

    // ------------------- ALERT DIALOG PO NACIŚNIĘCIU MARKERA -------------------------- //

    @Override
    public boolean onMarkerClick(Marker marker) {
        Cursor products = myProductDB.viewAllProducts();
        product_title = marker.getTitle();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle("Produkty");

        StringBuffer buffer = new StringBuffer();
        while (products.moveToNext()) {
            if(products.getString(3).equals(marker.getTitle())){
                buffer.append("Nazwa : " + products.getString(1) + " \n");
                buffer.append("Koszt : " + products.getString(2) + " \n\n");
            }
        }
        builder.setMessage(buffer.toString());
        builder.setPositiveButton("Usuń produkt", new DialogInterface.OnClickListener() {

            // ------------------- ALERT DIALOG PO NACIŚNIĘCIU USUŃ PRODUKT -------------------------- //

            @Override
            public void onClick(DialogInterface dialog, int which) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);
                builder.setCancelable(true);
                builder.setTitle("Podaj ID produktu");

                produkty = new ListView(MapsActivity.this);

                // --------------- USUWANIE PRODUKTU PO NACIŚNIĘCIU NA NIEGO-------------------------- //

                produkty.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        myProductDB.onDelete(produkty.getItemAtPosition(position).toString());
                        produktlist.remove(position);
                        adapter.notifyDataSetChanged();
                    }
                });

                Cursor products = myProductDB.viewAllProducts();

                produkty.setAdapter(adapter);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                produkty.setLayoutParams(lp);

                while (products.moveToNext()) {
                    if (products.getString(3).equals(product_title)) {
                        produktlist.add(products.getString(1));
                    }
                }

                builder.setView(produkty);

                builder.setNeutralButton("Powrót do mapy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        myProductDB.onDelete(Integer.parseInt(input.getText().toString()));
                    }
                });
                builder.show();
            }
        });
        builder.show();

        return false;
    }

    // ------------------- USUWANIE MARKERA PO PRZYTRZYMANIU  -------------------------- //

    @Override
    public void onMarkerDragStart(Marker marker) {
        myshopDB.onDelete(marker.getTitle());
        myProductDB.onDeleteAll(marker.getTitle());
        marker.remove();
    }

    public void onMarkerDrag(Marker marker){

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

    }
}
